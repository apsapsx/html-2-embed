/*
 * HTML 2 Embed
 * 2017 - Pablo Diehl
 * www.pablodiehl.xyz/api/html2embed
 */

/**
 * This function just remove all whites spaces and break lines from the String
 * 
 * str -> String to be compressed
 **/
function compress(str) {
    'use strict';
    
    str = str.split(/\>[\n\t\s]*\</g).join('><')
             .split(/[\t ]+\</g).join('<')
             .split(/\>[\t ]+s/g).join('>')
             .split(/\}[\t ]+s/g).join('}')
             .split(/[\t ]+\}/g).join('}')
             .split(/\{[\t ]/g).join('{')
             .split(/[\t ]+\{/g).join('{')
             .split(/ +(?= )/g).join('')
             .split(/[\n\t]*/gm).join('')
             .split(/\n/g).join('')
             .split('&@#break#@&').join('\r\n');
             
    return str;
}

/**
 * Do the same as the function above and also change the characters for its html codes,
 *     for example, & turns into &#38;
 * 
 +
 * str -> String to be compressed
 **/
function setChar(str, char) {
    'use strict';
    
    if (char) {
        return compress(str).replace(/[\u00A0-\u9999\<\>\&\'\"\\\/]/gim, function (c) {
            return '&#' + c.charCodeAt(0) + ';' ;
        });
    } else {
        return compress(str);
    }
}

/**
 * Do the same as the function above and also change the charactxtters for its html codes,
 *     for example, & turns into &#38;
 * 
 * str -> String to be compressed
 **/
function setEmbed(str, char, act) {
    'use strict';
    
    if (act) {
        str = compress(str).replace(/[\\]/g, '\\\\')
                 .replace(/"/g, '\\"');
    }

    if (char) {
        return str.replace(/[\u00A0-\u9999\<\>\&\'\"\\\/]/gim, function (c) {
            return '&#' + c.charCodeAt(0) + ';' ;
        });
    }
    
    return str;
}

var h2e = html2embed = function(){
    'use strict';
    var argLen = arguments.length;
    
    switch(argLen){
        case 1:
            return compress(arguments[0]);

        case 2:
            return setChar(arguments[0], arguments[1]);
        
        case 3:
            return setEmbed(arguments[0], arguments[1], arguments[2]);
            
        default:
            throw("HTML2Embed expects at one to three arguments!");
    }
};
